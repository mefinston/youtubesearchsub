from urllib.parse import urlparse, parse_qs
from youtube_transcript_api import YouTubeTranscriptApi
from models import Ysub
from pathlib import Path
from datetime import datetime
import json


URL_FILE = Path('urls')
SUB_FILE = Path("search")

if __name__ == "__main__":
    # Список ссылок видео
    urls_youtube = []
    if URL_FILE.exists():
        with open(URL_FILE, "r") as fp:
            for line in fp.readlines():
                urls_youtube.append(line)
    else:
        print("Файл urls не найден")
        exit(1)

    # Список искомых субтитров
    searchs = []
    if SUB_FILE.exists():
        with open(SUB_FILE, "r") as fp:
            for line in fp.readlines():
                searchs.append(line)
    else:
        print("Файл search не найден")
        exit(1)

    if len(urls_youtube) == 0:
        print("Нет ссылок youtube")
        exit(1)

    if len(searchs) == 0:
        print("Нет субтитров для поиска")
        exit(1)

    subs = {}
    ysubs = Ysub.select()
    vkeys = []
    for y in ysubs:
        vkeys.append(y.vkey)

    for url in urls_youtube:
        url_param = urlparse(url)
        qspars = parse_qs(url_param.query)
        vkey = qspars['v'][0]
        if vkey not in vkeys:
            ysub = Ysub()
            ysub.vkey = vkey
            ysub.url = url
            srt = YouTubeTranscriptApi.get_transcript(vkey, languages=['ru'])

            subs[url] = srt
            ysub.sub = json.dumps(srt)
            ysub.save()
        else:
            ysub = Ysub.get(Ysub.vkey == vkey)
            subs[ysub.url] = json.loads(ysub.sub)

    # искомое: {'video': url, "srt":  dict с субтитрами}
    """
    {"search":[
        {
            "video": "url",
            "srt": [line]
        }
    ]
    }
    """

    finded = {}

    # поиск
    for s in searchs:
        if s not in finded:
            finded[s] = []

        for url, sublines in subs.items():
            data = {
                'video': url,
                'srt': []
            }
            for line in sublines:
                if s in line['text']:
                    data['srt'].append(line)
            finded[s].append(data)

    dir_result = Path("result")
    if not dir_result.exists():
        dir_result.mkdir()

    filenamel = dir_result / Path('last.json')
    if not filenamel.exists():
        filenamel.touch()

    filenamed = dir_result / Path(f'{datetime.now()}.json')
    filenamed.touch()

    try:
        with open(filenamed, "w") as fp:
            json.dump(finded, fp, ensure_ascii=False)

        with open(filenamel, "w") as fp:
            json.dump(finded, fp, ensure_ascii=False)
    except (Exception, ):
        with open(filenamed, "w") as fp:
            fp.writelines(finded)

        with open(filenamel, "w") as fp:
            fp.writelines(finded)


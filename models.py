from peewee import *


db = SqliteDatabase('mydb.db')


class Ysub(Model):
    class Meta:
        database = db

    url = CharField()
    vkey = CharField()
    sub = TextField()


db.connect()
db.create_tables([Ysub,])

